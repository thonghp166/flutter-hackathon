import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/support/constants.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  final String descriptionAbout =
      "Ứng dụng giúp học sinh/sinh viên học tiếng Anh dựa trên cách học từ mới bằng flashcard, cùng những kỳ thi thú vị và đầy kịch tích, với những phần quá hấp dẫn như: cặp vé xem film CGV, điện thoại thông minh, ... ";

  @override
  initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 32, 0, 32),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: backgroundGradient)),
          child: SafeArea( child: Column(
            children: <Widget>[
              SizedBox(
                height: 200,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Padding(
                      child: Text("Giới thiệu",
                          style: TextStyle(
                              color: mainColorText,
                              fontSize: 22.0,
                              fontWeight: FontWeight.w600)),
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Padding(
                        child: Text(descriptionAbout,
                            style: TextStyle(
                                color: subColorText,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w400)),
                        padding: EdgeInsets.all(16.0))
                  ],
                )
              ),
              Card(
                color: Colors.transparent,
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(
                        Icons.home,
                        color: iconColor,
                      ),
                      title: Text(
                        "https://sachmem.vn",
                        style: TextStyle(color: mainColorText),
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.email,
                        color: iconColor,
                      ),
                      title: Text("lienhe@sachmem.vn",
                          style: TextStyle(color: mainColorText)),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.phone_iphone,
                        color: iconColor,
                      ),
                      title: Text("+842435122222",
                          style: TextStyle(color: mainColorText)),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(
                        FontAwesomeIcons.facebook,
                        color: iconColor,
                      ),
                      title: Text("Facebook",
                          style: TextStyle(color: mainColorText)),
                      onTap: () {},
                    )
                  ],
                ),
              ),
              Card(
                color: Colors.transparent,
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text("Phiên bản",
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              color: mainColorText)),
                    ),
                    ListTile(
                      title: Text(
                        "Phiên bản 1.0 - 10/2019",
                        style: TextStyle(fontSize: 14, color: mainColorText),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),),
        ),
        BackNavigation(context)
      ],
    ));
  }
}
