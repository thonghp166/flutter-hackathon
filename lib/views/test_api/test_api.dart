import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/core/services/chapter_service.dart';

class TestAPI extends StatefulWidget {
  @override
  _TestAPIState createState() => _TestAPIState();
}

class _TestAPIState extends State<TestAPI> {
  dynamic _consoleObject = 'Select api to test';
  int _currentApiIndex = -1;
  final List<Map> endpoints = [
    {
      'name': '/chapters',
      'value': ChapterService.instance.getChapters,
    },
    {
      'name': '/chapters/{id}/lessons',
      'value': ChapterService.instance.getLessons,
      'param': '5de7ed265d219e2dcb57f86f',
    }
  ];

  List<Widget> createApiList() {
    List<Widget> widgets = [];
    endpoints.asMap().forEach((i, e) => widgets.add(
          GestureDetector(
            onTap: () async {
              _consoleObject = 'Calling API...';
              _currentApiIndex = i;
              setState(() {});
              _consoleObject = (e['param'] == null) ? await e['value']() : await e['value'](e['param']);
              setState(() {});
            },
            child: Container(
              height: 40,
              color: (i == _currentApiIndex) ? Colors.green : Colors.transparent,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.add,
                    size: 24,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    e['name'],
                    style: TextStyle(fontSize: 18),
                  )
                ],
              ),
            ),
          ),
        ));
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    var consoleText = '';
    if (_consoleObject is List)
      (_consoleObject as List)
          .forEach((o) => consoleText += '\n' + jsonEncode(o));
    else
      consoleText = jsonEncode(_consoleObject);
    return Scaffold(
      appBar: AppBar(
        title: Text('Test API'),
        backgroundColor: Colors.redAccent,
      ),
      body: Column(children: [
        Container(
          height: 200,
          alignment: Alignment.topLeft,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: ListView(
            children: createApiList(),
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black,
            ),
            alignment: Alignment.topLeft,
            child: Text(
              consoleText,
              style: TextStyle(color: Colors.green),
            ),
          ),
        ),
      ]),
    );
  }
}
