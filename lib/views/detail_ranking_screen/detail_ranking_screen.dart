import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/ranking_screen/ranking_screen.dart';

class DetailRankingScreen extends StatefulWidget {
  @override
  _DetailRankingState createState() => _DetailRankingState();
}

class _DetailRankingState extends State<DetailRankingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: backgroundGradient)),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                _titleAndDescription(),
                _modalAndStarsBackground(),
              ],
            ),
          ),
        ),
        BackNavigation(context)
      ],
    ));
  }

  Widget _titleAndDescription() {
    return Container(
        padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                'Chúc mừng',
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.w600),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  'Bạn là người chiến thắng',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _modalAndStarsBackground() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 24, 0, 0),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
//              child: Image.network(
//                imageStarsBackground,
//                fit: BoxFit.fill,
//                width: MediaQuery.of(context).size.width,
//                height: MediaQuery.of(context).size.width,
//              ),
              child: Image.asset(
                'images/starsBackground.png',
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
            child: Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                width: 100,
                height: 100,
                child: Image.asset(
                  'images/modalIcon.png',
                  fit: BoxFit.fill,
                  width: 100,
                  height: 100,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 52, 0, 0),
            child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  "1",
                  style: TextStyle(
                      fontSize: 28.0,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                )),
          ),
          Padding(
              padding: EdgeInsets.fromLTRB(
                  0, MediaQuery.of(context).size.width * 0.5 + 36, 0, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      '100 điểm',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 240,
                        child: Container(
//                          color: Colors.red,
                          child: _giftView(),
                        ),
                      )),
                ],
              )),
        ],
      ),
    );
  }

  Widget _giftView() {
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0, 8, 0, 16),
          child: Align(
            alignment: Alignment.topCenter,
            child: Text(
              'Phần thưởng: 1 cặp vé CGV film bất kỳ',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Image.network(
            imageCgvTickets,
            width: MediaQuery.of(context).size.width * 0.5,
            height: 120,
          ),
        ),
        Row(
//          crossAxisAlignment: CrossAxisAlignment.baseline,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                height: 48,
                width: MediaQuery.of(context).size.width * 0.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  //color: continueButtonColor,
                ),
                child: RaisedButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RankingScreen()
                      )),
                  color: Colors.redAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  child: Center(
                    child: Text(
                      'Top Ranking',
                      style: TextStyle(color: mainColorText, fontSize: 16),
                    ),
                  ),
                ),
              ),
            ),Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                height: 48,
                width: MediaQuery.of(context).size.width * 0.4,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  //color: continueButtonColor,
                ),
                child: RaisedButton(
                  onPressed: () => {
//                    Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                            builder: (context) => RankingScreen()
//                        ))
                  },
                  color: Colors.redAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  child: Center(
                    child: Text(
                      'Nhận thưởng',
                      style: TextStyle(color: mainColorText, fontSize: 16),
                    ),
                  ),
                ),
              ),
            ),
          ],
        )



      ],
    ));
  }
}

//                      Align(
//                        alignment: Alignment.topCenter,
//                        child: SizedBox(
//                          width: 200,
//                          height: 200,
//                          child: CachedNetworkImage(
//                            imageUrl:
//                            avatarUserUrl,
//                            width: 200,
//                            height: 200,
//                            placeholder: (context, url) => new CircularProgressIndicator(),
//                            errorWidget: (context, url, error) => new Icon(Icons.error),
//                          ),
//                        ),
//                      ),
