import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/views/chapter/chapter_info.dart';
import 'package:gepochs/support/constants.dart';

import 'home/home.dart';

const ExampleData = [
  {
    'index': 1,
    'title': 'City',
    'imageUrl': 'https://media.timeout.com/images/105404217/630/472/image.jpg'
  },
  {
    'index': 2,
    'title': 'Cuisine',
    'imageUrl':
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRy36pdHUTbXMEYlMU6PKGKtnWW35AqdL-6PmjsvEIhbLbKXwn45A&s'
  },
  {
    'index': 3,
    'title': 'Transportation',
    'imageUrl':
        'https://cdn.givingcompass.org/wp-content/uploads/2018/11/26103223/Finding-Solutions-for-Americas-Transportation-Dilemma.jpg'
  },
  {
    'index': 4,
    'title': 'Culture',
    'imageUrl':
        'https://www.holidify.com/images/cmsuploads/compressed/carnaval-gal-3_20171223222035.jpg'
  },
  {
    'index': 5,
    'title': 'Environment',
    'imageUrl':
        'https://www.nationwiderepairs.co.uk/wp-content/uploads/2018/07/helping-the-environment-2.jpg'
  },
  {
    'index': 6,
    'title': 'Work',
    'imageUrl':
        'https://assets.entrepreneur.com/content/3x2/2000/20190905183829-GettyImages-908886812.jpeg?width=700&crop=2:1'
  },
  {
    'index': 7,
    'title': 'Technology',
    'imageUrl':
        'https://storage.googleapis.com/afs-prod/media/afs:Medium:6906200015/775.jpeg'
  },
  {
    'index': 8,
    'title': 'Space',
    'imageUrl':
        'https://spacenews.com/wp-content/uploads/2018/05/24359364107_152b0152ff_k-879x485.jpg'
  },
  {
    'index': 9,
    'title': 'Education',
    'imageUrl':
        'https://www.thebalancecareers.com/thmb/3TXSZvKKQXcWu3Zu3qU8ag158MU=/3864x2577/filters:fill(auto,1)/school-books-on-desk--education-concept-871454068-5b548900c9e77c005b04fc8c.jpg'
  },
  {
    'index': 10,
    'title': 'Movies',
    'imageUrl':
        'https://www.destinationgoldcoast.com/blog/wp-content/uploads/HOTA-cinema.jpg'
  },
];

class ChapterList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(32, 64, 32, 32),
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: backgroundGradient)),
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    'Select chapter',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Expanded(
                  child: ListView(
                    children: ExampleData.map((item) => ChapterCard(
                          title: item['title'],
                          index: item['index'],
                          imageUrl: item['imageUrl'],
                        )).toList(),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(16),
                child: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    FontAwesomeIcons.arrowLeft,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ChapterCard extends StatelessWidget {
  final int index;
  final String title;
  final String imageUrl;
  final Color backgroundColor;
  static const _borderRadius = BorderRadius.all(Radius.circular(12));

  ChapterCard({this.index, this.title, this.imageUrl, this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
      padding: EdgeInsets.symmetric(horizontal: 4),
      height: 80,
      decoration: BoxDecoration(
        //boxShadow: box_shadow_2,
        borderRadius: _borderRadius,
      ),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              boxShadow: box_shadow_2,
              borderRadius: _borderRadius,
              image: DecorationImage(
                image: NetworkImage(imageUrl),
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Opacity(
            opacity: 0.5,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: _borderRadius,
                color: Colors.black,
//                border: Border.all(color: splashColors[index-1], width: 4),
              ),
            ),
          ),
          RaisedButton(
            elevation: 0,
            focusElevation: 0,
            hoverElevation: 0,
            highlightElevation: 0,
            splashColor: splashColors[index - 1],
            color: Colors.transparent,
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChapterInfo(
                          index: index,
                          title: title,
                          imageUrl: imageUrl,
                          color: splashColors[index - 1],
                        ))),
            shape: RoundedRectangleBorder(
              side: BorderSide(color: splashColors[index - 1], width: 4),
              borderRadius: _borderRadius,
            ),
            child: Center(
              child: Text(
                'Chapter $index: $title',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          )
        ],
      ),
    );
  }
}
