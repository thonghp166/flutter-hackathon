import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/home/contest_card.dart';
import 'package:gepochs/views/quiz/quiz_exercise.dart';

class StartContestScreen extends StatefulWidget {
  @override
  _StartContestState createState() => _StartContestState();
}

class _StartContestState extends State<StartContestScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 16, 0, 32),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: backgroundGradient)),
          child: SafeArea(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    'Start Contest',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(16, 64, 16, 0),
                  height: 240,
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: ContestCard(
                        title: 'Food', reward: 'Something', position: 1),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(16, 280, 16, 0),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              'Phần thưởng: 1 cặp vé CGV film bất kỳ',
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
                          child: Center(
                            child: Image.network(
                              imageCgvTickets,
                              width: MediaQuery.of(context).size.width * 0.5,
                              height: 120,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: MediaQuery.of(context).size.width / 2 - 80,
                  child: Container(
                    alignment: Alignment.center,
                    height: 60,
                    width: 160,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      //color: continueButtonColor,
                    ),
                    child: RaisedButton(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => QuizExercise())),
                      color: continueButtonColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                      ),
                      child: Center(
                        child: Text(
                          'Start Contest',
                          style:
                              TextStyle(color: buttonTextColor, fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        BackNavigation(context)
      ],
    ));
  }
}
