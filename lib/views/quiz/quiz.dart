import 'package:flutter/cupertino.dart';
import 'package:gepochs/views/quiz/question.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';

import 'answer.dart';

class Quiz extends StatefulWidget {
  final QuizData data;
  final Function callback;
  final answerWidth;
  final playback;

  Quiz({Key key, this.data, this.callback, this.answerWidth, this.playback = Playback.START_OVER_FORWARD}) : super(key: key);

  @override
  QuizState createState() => QuizState();
}

class QuizState extends State<Quiz> {
  @override
  Widget build(BuildContext context) {
    var answerWidgets = [];
    var options = ['A', 'B', 'C', 'D'];
    var answerIndex = 0;
    for (var answer in widget.data.answers.keys) {
      var answerWidget = Answer(
        option: options.removeAt(0),
        answer: answer,
        width: widget.answerWidth,
        playback: widget.playback,
        delay: answerIndex,
        duration: 500 + answerIndex * 150,
        callback: widget.callback,
      );
      answerWidgets.addAll([
        answerWidget,
        SizedBox(
          height: 8,
        )
      ]);
      answerIndex++;
    }
    var col = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Question(
            question: widget.data.question,
          ),
          SizedBox(
            height: 16,
          ),
          ...answerWidgets
        ]);
    return col;
  }
}

class QuizData {
  final String question;
  final Map answers;

  QuizData({this.question, this.answers});
}
