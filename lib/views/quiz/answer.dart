import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/views/common-widgets/animations.dart';
import 'package:simple_animations/simple_animations.dart';

class Answer extends StatelessWidget {
  final Color color;
  final String option;
  final String answer;
  final double width;
  final Playback playback;
  final int duration;
  final int delay;
  final Function callback;

  Answer(
      {this.color,
      this.option,
      this.answer,
      this.width = 300,
      this.playback = Playback.PLAY_FORWARD,
      this.delay = 0,
      this.duration = 500,
      this.callback});

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(color: Colors.white, fontSize: 18);
    return Grow(
      begin: Offset(60, 60),
      end: Offset(width, 60),
      playback: playback,
      duration: Duration(milliseconds: duration + 150 * delay),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: RaisedButton(
          onPressed: () {
            if (callback != null) callback(true, answer);
          },
          splashColor: Colors.greenAccent,
          color: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(30)),
          ),
          child: Container(
            alignment: Alignment.centerLeft,
            child: Text(
              '${option.toUpperCase()}. $answer',
              style: style,
              softWrap: false,
              overflow: TextOverflow.clip,
            ),
          ),
        ),
      ),
    );
  }
}
