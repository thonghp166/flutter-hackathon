import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/views/detail_ranking_screen/detail_ranking_screen.dart';
import 'package:gepochs/views/quiz/quiz.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:gepochs/support/constants.dart';

class QuizExercise extends StatefulWidget {
  @override
  _QuizExerciseState createState() => _QuizExerciseState();
}

class _QuizExerciseState extends State<QuizExercise> {
  final List<QuizData> _quizData = [
    QuizData(
      question: '1 plus 1 equals ...',
      answers: {
        '2': true,
        '5': false,
        '4': false,
        '0': false,
      },
    ),
    QuizData(
      question: '2 plus 3 equals ...',
      answers: {
        '4': false,
        '6': false,
        '5': true,
        '3': false,
      },
    ),
    QuizData(
      question: '3 minus 2 equals ...',
      answers: {
        '0': false,
        '2': false,
        '-1': false,
        '1': true,
      },
    ),
  ];

  int _currentQuizIndex = 0;
  Quiz _currentQuiz;
  bool _showResult = false;
  bool _correctAnswer = false;

  _nextQuiz(showResult, answer) async {
    if (showResult) {
      _showResult = showResult;
      _correctAnswer = _currentQuiz.data.answers[answer];
      setState(() {});
      await Future.delayed(Duration(milliseconds: 500));
    }
    if (_currentQuizIndex == _quizData.length - 1) {
//      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => DetailRankingScreen()
      ));
    }
    else {
      _showResult = false;
      _currentQuizIndex++;
      setState(() {});
    }
  }

  Quiz _createQuiz(answerWidth) {
    _currentQuiz = Quiz(
      data: _quizData[_currentQuizIndex],
      answerWidth: answerWidth,
      callback: _nextQuiz,
      playback: (_showResult) ? Playback.PAUSE : Playback.START_OVER_FORWARD,
    );
    return _currentQuiz;
  }

  @override
  Widget build(BuildContext context) {
    double horizontalPadding = 36;
    var screenWidth = MediaQuery.of(context).size.width;
    var answerWidth = screenWidth - horizontalPadding * 2;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: backgroundGradient)),
            child: Center(
              child: _createQuiz(answerWidth),
            ),
          ),
          SafeArea(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Text(
                        '${_currentQuizIndex + 1}/${_quizData.length}',
                        style: TextStyle(color: Colors.white, fontSize: 24),
                      )),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    alignment: Alignment.bottomRight,
                    child: Container(
                      width: 120,
                      height: 48,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(24)),
                        ),
                        onPressed: () {
                          _nextQuiz(false, null);
                        },
                        color: Colors.blueGrey,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.skip_next,
                              color: Colors.white,
                              size: 28,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Skip',
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: (_showResult)
                      ? Icon(
                    (_correctAnswer) ? Icons.check : Icons.close,
                    color: (_correctAnswer)
                        ? Colors.greenAccent
                        : Colors.redAccent,
                    size: 96,
                  )
                      : null,
                ),
              ],
            )
          ),
          BackNavigation(context),
        ],
      ),
    );
  }
}
