import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/views/common-widgets/animations.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:gepochs/support/constants.dart';

class Question extends StatelessWidget {
  final String question;
  final List<String> answers;
  final String correctAnswer;

  Question({this.question, this.answers, this.correctAnswer});

  @override
  Widget build(BuildContext context) {
    return FadeIn(
      duration: Duration(milliseconds: 800),
      playback: Playback.START_OVER_FORWARD,
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: backgroundGradient),
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Text(
          question,
          style: TextStyle(color: Colors.white, fontSize: 24),
        ),
      ),
    );
  }
}
