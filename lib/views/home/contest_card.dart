import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/views/start_contest_screen/start_contest_screen.dart';

class ContestCard extends StatelessWidget {
  static final TextStyle titleStyle = TextStyle(
    color: Colors.white,
  );
  static final TextStyle subtitleStyle = TextStyle(color: Colors.white);
  static final TextStyle rankingStyle = TextStyle(color: Colors.green);

  final String title;
  final String reward;
  final int position;

  ContestCard({this.title, this.reward, this.position});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: RaisedButton(
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => StartContestScreen()
            ));
          },
          color: Colors.transparent,
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(8))),
          child: Column(
            children: [
              ListTile(
                leading: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.orange),
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      FontAwesomeIcons.trophy,
                      color: Colors.orange,
                    )),
                title: Text(
                  title,
                  style: titleStyle,
                ),
                subtitle: Row(
                  children: [
                    Text(
                      'Your position: ',
                      style: subtitleStyle,
                    ),
                    Text(
                      '$position',
                      style: rankingStyle,
                    )
                  ],
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 8, 8),
                    child: Text(
                      'Reward: $reward',
                      style: titleStyle,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      );
  }
}
