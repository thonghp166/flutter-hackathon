import 'dart:ui';

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/core/models/user.dart';
import 'package:gepochs/views/chapter/chapter_info.dart';
import 'package:gepochs/views/chapter_list.dart';
import 'package:gepochs/views/common-widgets/animations.dart';
import 'package:gepochs/views/common-widgets/icon_buttons.dart';
import 'package:gepochs/views/login_screen/authentication.dart';
import 'package:gepochs/views/settings_screen/settings_screen.dart';
import 'package:gepochs/views/user_profile_screen/user_profile_screen.dart';
import 'package:gepochs/views/home/contest_card.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:gepochs/support/constants.dart';

const decoration_image_url =
    'https://media.timeout.com/images/105404217/630/472/image.jpg';

//const box_shadow_1 = [BoxShadow(color: Colors.black26, offset: Offset(2, 2))];
//const box_shadow_2 = [BoxShadow(color: Colors.black38, offset: Offset(4, 4))];
//const box_shadow_3 = [BoxShadow(color: Colors.black26, offset: Offset(8, 8))];
const box_shadow_1 = <BoxShadow>[];
const box_shadow_2 = <BoxShadow>[];
const box_shadow_3 = <BoxShadow>[];

final example = ExampleData[0];

class Home extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback onSignedOut;
  final User user;

  const Home({Key key, this.auth, this.onSignedOut, this.user}) : super(key: key);
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  bool _showMenu = false;
  int _navigationIndex = 0;
  bool _renderAnimation = true;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _renderAnimation = false;
    });
  }
  void logout() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        child: Stack(
          children: <Widget>[
            // first stack
            Container(
              decoration: BoxDecoration(
                color: backgroundGradient[1],
                gradient: LinearGradient(colors: backgroundGradient),
              ),
              child: SafeArea(
                child: Fly(
                  duration: const Duration(milliseconds: 800),
//                delay: 5000,
                  transfer: 25,
                  flyDirection: FlyDirection.Vertical,
                  flyAnimation: FlyAnimation.FlyIn,
                  startOver: _renderAnimation,
                  child: FadeIn(
                    duration: Duration(milliseconds: 800),
                    child:
                        (_navigationIndex == 0) ? CareerBody() : ContestBody(),
                    playback: (_renderAnimation)
                        ? Playback.START_OVER_FORWARD
                        : Playback.PLAY_FORWARD,
                  ),
                ),
              ),
            ),
            // second stack
            SafeArea(
              child: Stack(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(16, 16, 0, 16),
                        child: GestureDetector(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserProfileScreen(auth: widget.auth, user: widget.user,))),
                          child: Container(
                            width: 56,
                            height: 56,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white),
                                shape: BoxShape.circle,
                                boxShadow: box_shadow_2,
                                image: DecorationImage(
                                  image: NetworkImage(widget.user.imgUrl),
                                  fit: BoxFit.fill,
                                )),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListTile(
                          title: Text(
                            widget.user.name,
                            style: TextStyle(color: headerTextColor),
                          ),
                          subtitle: Row(
                            children: <Widget>[
                              Icon(
                                FontAwesomeIcons.coins,
                                color: Colors.orangeAccent,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                widget.user.coin.toString(),
                                style: TextStyle(color: headerTextColor),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  // third stack
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 16, 8, 12),
                                  child: RotateIconButton(
                                    iconData: FontAwesomeIcons.ellipsisH,
                                    backgroundColor: iconButtonColor,
                                    onPressed: () {
                                      _showMenu = !_showMenu;
                                      setState(() {});
                                    },
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 8, 12),
                                  child: Fly(
                                    flyAnimation: (_showMenu)
                                        ? FlyAnimation.FlyIn
                                        : FlyAnimation.FlyOut,
                                    duration: Duration(milliseconds: 80 * 3),
                                    child: MyIconButton(
                                      iconData: FontAwesomeIcons.cog,
                                      backgroundColor: iconButtonColor,
                                      onPressed: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SettingsScreen(auth: widget.auth,user:widget.user))),
                                    ),
                                  )),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
                                child: Fly(
                                  flyAnimation: (_showMenu)
                                      ? FlyAnimation.FlyIn
                                      : FlyAnimation.FlyOut,
                                  duration: Duration(milliseconds: 80 * 5),
                                  child: ReplaceIconButton(
                                    iconData: FontAwesomeIcons.volumeUp,
                                    replaceIconData:
                                        FontAwesomeIcons.volumeMute,
                                    backgroundColor: iconButtonColor,
                                  ),
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 8, 8, 0),
                                  child: Fly(
                                    flyAnimation: (_showMenu)
                                        ? FlyAnimation.FlyIn
                                        : FlyAnimation.FlyOut,
                                    duration: Duration(milliseconds: 80 * 7),
                                    child: MyIconButton(
                                      iconData: Icons.exit_to_app,
                                      backgroundColor: iconButtonColor,
                                      onPressed: logout,
                                    ),
                                  )
                              )
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: FancyBottomNavigation(
        circleColor: backgroundGradient[0],
        barBackgroundColor: backgroundGradient[1],
        textColor: Colors.white,
        inactiveIconColor: Colors.white,
        activeIconColor: Colors.white,
        tabs: [
          TabData(iconData: FontAwesomeIcons.school, title: 'Career'),
          TabData(iconData: FontAwesomeIcons.flagCheckered, title: 'Contest')
        ],
        onTabChangedListener: (index) {
          setState(() {
            _navigationIndex = index;
            _renderAnimation = true;
            Future.delayed(Duration(milliseconds: 200)).then((_) {
              setState(() {
                _renderAnimation = false;
              });
            });
          });
        },
      ),
    );
  }
}

class CareerBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
      child: ListView(
        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        children: <Widget>[
          Container(
            child: Text(
              'Career',
              style: TextStyle(color: headerTextColor, fontSize: 48),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                child: Text(
                  'Completion:',
                  style: TextStyle(color: headerTextColor, fontSize: 24),
                ),
              ),
              Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  border: Border.all(color: completionIconBorderColor),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Text(
                    '2%',
                    style: TextStyle(fontSize: 20, color: buttonTextColor),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: <Widget>[
              Container(
                width: 96,
                height: 32,
                decoration: BoxDecoration(
                    boxShadow: box_shadow_2,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: RaisedButton(
                  color: browseButtonColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(18)),
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChapterList())),
                  child: Center(
                    child: Text(
                      'Browse...',
                      style: TextStyle(color: buttonTextColor, fontSize: 14),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 24,
          ),
          Container(
            height: 250,
            decoration: BoxDecoration(
                //border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.all(Radius.circular(24)),
                color: Colors.white,
                boxShadow: box_shadow_2),
            child: Stack(
              children: [
                Container(
                  child: Opacity(
                    opacity: 1,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(24)),
                          image: DecorationImage(
                            image: NetworkImage(decoration_image_url),
                            fit: BoxFit.fill,
                          )),
                    ),
                  ),
                ),
                Container(
                  child: Opacity(
                    opacity: 0.5,
                    child: Container(
                      height: 150,
                      decoration: BoxDecoration(
                          color: currentLessonColor,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(24),
                            topRight: Radius.circular(24),
                            bottomRight: Radius.circular(80),
                          )),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Chapter 1: City',
                        style: TextStyle(color: headerTextColor, fontSize: 34),
                      ),
                      Divider(
                        thickness: 3,
                        color: Colors.redAccent,
                      ),
                      Text(
                        'Some description about chapter 1',
                        style: TextStyle(color: headerTextColor, fontSize: 20),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 24,
          ),
          Center(
            child: Container(
              height: 60,
              width: 160,
              decoration: BoxDecoration(
                boxShadow: box_shadow_2,
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              child: RaisedButton(
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChapterInfo(
                              index: example['index'],
                              title: example['title'],
                              imageUrl: example['imageUrl'],
                              color:
                                  splashColors[(example['index'] as int) - 1],
                            ))),
                color: continueButtonColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                child: Center(
                  child: Text(
                    'Continue',
                    style: TextStyle(color: buttonTextColor, fontSize: 24),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 48,
          ),
        ],
      ),
    );
  }
}

class ContestBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
      child: ListView(
        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        children: <Widget>[
          Container(
            child: Text(
              'Contest',
              style: TextStyle(color: headerTextColor, fontSize: 48),
            ),
          ),
          ContestCard(title: 'Food', reward: 'Something', position: 1),
          ContestCard(
            title: 'City',
            reward: 'Something else',
            position: 5,
          ),
          ContestCard(
            title: 'School',
            reward: 'Something else',
            position: 1000,
          ),
          ContestCard(
            title: 'Environment',
            reward: 'Something else',
            position: 226,
          ),
          ContestCard(
            title: 'Architecture',
            reward: 'Something else',
            position: 65,
          ),
          SizedBox(
            height: 80,
          )
        ],
      ),
    );
  }
}
