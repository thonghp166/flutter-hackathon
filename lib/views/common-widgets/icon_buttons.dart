import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

const double ICON_SIZE = 24.0;
const double ICON_BUTTON_SIZE = 56.0;
const Color ICON_COLOR = Colors.white;
const Color ICON_BUTTON_COLOR = Colors.redAccent;
const BOX_SHADOW = <BoxShadow>[
//  BoxShadow(
//    color: Colors.black26,
//    //blurRadius: 20.0,
//    //spreadRadius: 5.0,
//    offset: Offset(4.0, 4.0),
//  )
];

class MyIconButton extends StatelessWidget {
  final IconData iconData;
  final Color iconColor;
  final double iconSize;
  final Color backgroundColor;
  final double size;
  final Function onPressed;

  MyIconButton(
      {this.iconData,
      this.iconColor = ICON_COLOR,
      this.iconSize = ICON_SIZE,
      this.backgroundColor = ICON_BUTTON_COLOR,
      this.size = ICON_BUTTON_SIZE,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
//      decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: BOX_SHADOW),
      child: RaisedButton(
        onPressed: onPressed,
        color: backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(size / 2)),
        ),
        child: Center(
          child: Icon(
            iconData,
            size: iconSize,
            color: iconColor,
          ),
        ),
      ),
    );
  }
}

class RotateIconButton extends StatefulWidget {
  final IconData iconData;
  final Color iconColor;
  final double iconSize;
  final Color backgroundColor;
  final double size;
  final Function onPressed;

  RotateIconButton(
      {Key key,
      this.iconData,
      this.iconColor = ICON_COLOR,
      this.iconSize = ICON_SIZE,
      this.backgroundColor = ICON_BUTTON_COLOR,
      this.size = ICON_BUTTON_SIZE,
      this.onPressed});

  @override
  _RotateButtonIconState createState() => _RotateButtonIconState();
}

class _RotateButtonIconState extends State<RotateIconButton> {
  bool _forward = false;
  bool _rotate = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _rotate = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track('rotate')
          .add(Duration(milliseconds: 200), Tween(begin: 0.0, end: pi / 2))
    ]);
    return Container(
      width: widget.size,
      height: widget.size,
//      decoration: BoxDecoration(
//        shape: BoxShape.circle,
//        boxShadow: BOX_SHADOW,
//      ),
      child: RaisedButton(
        color: widget.backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(widget.size / 2)),
        ),
        onPressed: () {
          //print('clicked');
          _forward = !_forward;
          setState(() {});
          if (widget.onPressed != null) widget.onPressed();
        },
        child: Center(
          child: (_rotate)
              ? ControlledAnimation(
                  playback: (_forward)
                      ? Playback.PLAY_FORWARD
                      : Playback.PLAY_REVERSE,
                  duration: tween.duration,
                  tween: tween,
                  builder: (context, animation) {
                    return Transform.rotate(
                      angle: animation['rotate'],
                      child: Icon(
                        widget.iconData,
                        size: widget.iconSize,
                        color: widget.iconColor,
                      ),
                    );
                  })
              : Icon(
                  widget.iconData,
                  size: widget.iconSize,
                  color: widget.iconColor,
                ),
        ),
      ),
    );
  }
}

class ReplaceIconButton extends StatefulWidget {
  final IconData iconData;
  final IconData replaceIconData;
  final double iconSize;
  final Color iconColor;
  final Color backgroundColor;
  final double size;
  final Function onPressed;

  ReplaceIconButton(
      {Key key,
      this.iconData,
      this.replaceIconData,
      this.iconSize = ICON_SIZE,
      this.iconColor = ICON_COLOR,
      this.backgroundColor = ICON_BUTTON_COLOR,
      this.size = ICON_BUTTON_SIZE,
      this.onPressed});

  @override
  _ReplaceIconButton createState() => _ReplaceIconButton();
}

class _ReplaceIconButton extends State<ReplaceIconButton> {
  bool clicked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size,
      height: widget.size,
//      decoration: BoxDecoration(
//          shape: BoxShape.circle,
//          color: widget.backgroundColor,
//          boxShadow: BOX_SHADOW),
      child: RaisedButton(
        onPressed: () {
          setState(() {
            clicked = !clicked;
          });
          widget.onPressed();
        },
        color: widget.backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(widget.size / 2)),
        ),
        child: Center(
          child: Icon(
            (clicked) ? widget.replaceIconData : widget.iconData,
            color: widget.iconColor,
            size: widget.iconSize,
          ),
        ),
      ),
    );
  }
}
