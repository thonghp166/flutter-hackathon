import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

final emptyBox = SizedBox(
  width: 0,
  height: 0,
);

class FadeIn extends StatelessWidget {
  final Duration delayDuration;
  final Widget child;
  final Playback playback;
  final Duration duration;

  FadeIn(
      {Key key,
      this.duration = const Duration(milliseconds: 500),
      this.delayDuration = const Duration(seconds: 0),
      this.child,
      this.playback = Playback.PLAY_FORWARD})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track('opacity').add(duration, Tween(begin: 0.0, end: 1.0)),
    ]);
    return ControlledAnimation(
      playback: playback,
      delay: this.delayDuration,
      duration: tween.duration,
      tween: tween,
      builder: (context, animation) =>
          Opacity(opacity: animation['opacity'], child: child),
    );
  }
}

class Fly extends StatelessWidget {
  final Widget child;
  final double transfer;
  final FlyDirection flyDirection;
  final FlyAnimation flyAnimation;
  final Duration duration;
  final int delay;
  final bool startOver;

  Fly({
    Key key,
    this.duration = const Duration(milliseconds: 500),
    this.delay = 0,
    this.child,
    this.flyAnimation,
    this.transfer = 100,
    this.flyDirection = FlyDirection.Horizontal,
    this.startOver = false,
  });

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track('transfer').add(duration, Tween(begin: transfer, end: 0.0)),
    ]);
    var playback;
    if (flyAnimation == FlyAnimation.FlyIn)
      playback =
          (startOver) ? Playback.START_OVER_FORWARD : Playback.PLAY_FORWARD;
    else
      playback = Playback.PLAY_REVERSE;
    return ControlledAnimation(
      playback: playback,
      delay: Duration(milliseconds: delay),
      duration: tween.duration,
      tween: tween,
      builder: (context, animation) {
        //print('$transfer $flyDirection');
        return Transform.translate(
            offset: (flyDirection == FlyDirection.Horizontal)
                ? Offset(animation['transfer'], 0)
                : Offset(0, animation['transfer']),
            child: child);
      },
    );
  }
}

enum FlyDirection { Vertical, Horizontal }
enum FlyAnimation { FlyIn, FlyOut }

class Grow extends StatelessWidget {
  final Duration delayDuration;
  final Widget child;
  final Offset begin;

  final Offset end;
  final Playback playback;
  final Duration duration;

  Grow(
      {Key key,
      this.child,
      this.delayDuration,
      this.begin = const Offset(0, 0),
      this.end = const Offset(100, 100),
      this.playback = Playback.PLAY_FORWARD,
      this.duration = const Duration(milliseconds: 500)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track('size').add(duration, Tween<Offset>(begin: begin, end: end),
          curve: Curves.fastOutSlowIn),
    ]);
    return ControlledAnimation(
      playback: playback,
      delay: this.delayDuration,
      duration: tween.duration,
      tween: tween,
      builder: (context, animation) {
        //print('${animation['size'].dx}/${animation['size'].dy}');
        return Container(
          width: animation['size'].dx,
          height: animation['size'].dy,
          child: this.child,
        );
      },
    );
  }
}
