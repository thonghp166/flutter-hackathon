import 'package:flutter/material.dart';
import 'package:gepochs/views/about_screen/about_screen.dart';
import 'package:gepochs/views/ranking_screen/ranking_screen.dart';
import 'package:gepochs/views/settings_screen/settings_screen.dart';
import 'package:gepochs/views/user_profile_screen/user_profile_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const continueButtonColor = Colors.redAccent;
const buttonTextColor = Colors.white;

class HomeScreen extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  static const borderRadius = BorderRadius.all(Radius.circular(20));
  final int index = 0;
  final String title = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text("Home"),
//      ),
      key: _scaffoldKey,
      body: _bodyHome(),
      drawer: _drawer(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _scaffoldKey.currentState.openDrawer(),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _bodyHome(){
    return Stack(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Container(
                height: 60,
                width: 160,
                decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.all(Radius.circular(30)),
                  //color: continueButtonColor,
                ),
                child: RaisedButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RankingScreen()
                      )),
                  color: continueButtonColor,
                  shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(30)),
                  ),
                  child: Center(
                    child: Text(
                      'Ranking',
                      style: TextStyle(
                          color: buttonTextColor, fontSize: 24),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16),
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Icon(
                  FontAwesomeIcons.arrowLeft,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _drawer() {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                DrawerHeader(
                  child: Text(
                    "EZ Learning",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.book),
                  title: Text('Sách'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.people),
                  title: Text('Người dùng'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserProfileScreen()),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.info),
                  title: Text('Giới thiệu'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AboutScreen()),
                    );
                  },
                ),
              ],
            ),
          ),
          // This container holds the align
          Container(
              // This align moves the children to the bottom
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  // This container holds all the children that will be aligned
                  // on the bottom and should not scroll with the above ListView
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      Divider(),
                      ListTile(
                        leading: Icon(Icons.settings),
                        title: Text('Settings'),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SettingsScreen()),
                          );
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.help),
                        title: Text('Help and Feedback'),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: Text("Version: 1.0"),
                      ),
                    ],
                  ))))
        ],
      ),
    );
  }
}
