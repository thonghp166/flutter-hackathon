import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/views/login_screen/login.dart';
import 'package:gepochs/views/quiz/quiz_exercise.dart';
import 'package:gepochs/views/rank_screen/ranking_home.dart';
import 'package:gepochs/views/contest_result_screen/contest_result_screen.dart';
import 'package:gepochs/views/home/home.dart';
import 'package:gepochs/views/learning_screen/learning_screen.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/test_api/test_api.dart';

class ListViewTestScreen extends StatefulWidget {
  @override
  _ListViewTestState createState() => _ListViewTestState();
}

class _ListViewTestState extends State<ListViewTestScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(32, 16, 32, 32),
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: backgroundGradient)),
              child: listViewTest(context),
            ),
            BackNavigation(context)
          ],
        )
    );
  }

  Widget listViewTest(context) {
    return Stack(
      children: <Widget>[
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text("Login"),
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => LoginScreen())),
              ),
              RaisedButton(
                child: Text("Home"),
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Home()),
                ),
              ),
              RaisedButton(
                child: Text("Chapter"),
                onPressed: () => {},
              ),
              RaisedButton(
                child: Text("Learn"),
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => LearningScreen()),
                ),
              ),
              RaisedButton(
                child: Text("Contest"),
                onPressed: () => {},
              ),
              RaisedButton(
                child: Text("Contest Result"),
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ContestResultScreen()),
                ),
              ),
              RaisedButton(
                child: Text("Prize"),
                onPressed: () => {},
              ),
              RaisedButton(
                child: Text("Ranking"),
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => HomeScreen())),
              ),
              RaisedButton(
                child: Text('Quiz'),
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => QuizExercise())),
              ),
              RaisedButton(
                child: Text('Test API'),
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => TestAPI())),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
