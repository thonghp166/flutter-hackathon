import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gepochs/core/models/user.dart';
import 'package:gepochs/core/utilities/http_service.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class BaseAuth  {

  Future<String> signInFB();

  Future<String> signInGG();

  Future<User> getCurrentUser();

  Future<void> signOut();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final String endpoint = 'auths/signin/';
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final facebookLogin = new FacebookLogin();

  @override
  Future<User> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    if (user == null) return null;
    dynamic response = await HttpService.get(endpoint: "users/search?socialId="+ user.uid);
    return createUser(response, user);
  }

  @override
  Future<String> signInFB() async {
    final facebookLogin = new FacebookLogin();
    final res = await facebookLogin.logIn(['email']);
    final AuthCredential credential = FacebookAuthProvider.getCredential(accessToken: res.accessToken.token);
    final FirebaseUser firebaseUser = (await _firebaseAuth.signInWithCredential(credential)).user;
    SocialId socialId =
    SocialId(provider: "facebook", id: firebaseUser.uid);
    dynamic response = await HttpService.post(
        endpoint: endpoint, body: {"socialId": socialId.toJson()});
    return firebaseUser.uid;
  }

  @override
  Future<String> signInGG() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    FirebaseUser firebaseUser =
        (await _firebaseAuth.signInWithCredential(credential)).user;
    SocialId socialId = SocialId(provider: "google", id: firebaseUser.uid);
    await HttpService.post(
        endpoint: endpoint, body: {"socialId": socialId.toJson()});
    return firebaseUser.uid;
  }

  @override
  Future<void> signOut() async {
    await googleSignIn.signOut();
    await facebookLogin.logOut();
    return await _firebaseAuth.signOut();
  }

  User createUser(dynamic response, FirebaseUser firebaseUser) {
    if (response != null) {
      Map<String, dynamic> data = response['data'];
      return new User(
          id: data['_id'],
          name: firebaseUser.displayName,
          coin: data['coin'],
          uid: firebaseUser.uid,
          provider: 'google',
          imgUrl: firebaseUser.photoUrl,
          email: firebaseUser.email);
    }
  }


}