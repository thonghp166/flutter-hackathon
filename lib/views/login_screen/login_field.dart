import 'package:flutter/material.dart';

class LoginField extends StatelessWidget {

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return
      Container(
          alignment: Alignment.topLeft,
          width: double.infinity,
          child: Padding(
              padding: EdgeInsets.only(left: 0, right: 0, top: 15),
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Login",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontFamily: "Poppins-Bold",
                            letterSpacing: 1),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Email",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontFamily: "Poppins-Medium",
                        ),
                      ),
                      TextFormField(
                        style: TextStyle(
                            color: Colors.white
                        ),
                        key: Key("email"),
                        decoration: InputDecoration(
                            hintText: "Enter your email",
                            hintStyle: TextStyle(
                                color: Colors.grey, fontSize: 12.0),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white))),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Password",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontFamily: "Poppins-Medium",
                        ),
                      ),
                      TextFormField(
                        style: TextStyle(
                            color: Colors.white
                        ),
                        key: Key("password"),
                        obscureText: true,
                        decoration: InputDecoration(
                            hintText: "Enter your password",
                            hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          InkWell(
                            child: Container(
                                width: 150,
                                height: 50,
                                decoration: BoxDecoration(
                                  gradient:
                                  LinearGradient(colors: [
                                    Color(0xFF17ead9),
                                    Color(0xFF6078ea),
                                  ]),
                                  borderRadius:
                                  BorderRadius.circular(6.0),
                                ),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    //                              key: Key("signIn"),
                                    //                                  onTap: validateAndSubmit,
                                    child: Center(
                                        child: Text("SIGN IN",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily:
                                                "Poppins-Bold",
                                                fontSize: 18,
                                                letterSpacing:
                                                1.0))),
                                  ),
                                )),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )));
  }

}