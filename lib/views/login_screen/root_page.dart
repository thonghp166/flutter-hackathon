import 'package:flutter/material.dart';
import 'package:gepochs/core/models/user.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/home/home.dart';
import 'package:gepochs/views/login_screen/login.dart';

import 'authentication.dart';

enum AuthStatus { LOGGED_IN, NOT_LOGGED_IN, NOT_DETERMINED }

class RootPage extends StatefulWidget {
  RootPage({this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String userId = "";
  User curUser;

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          userId = user?.uid;
          curUser = user;
        }
        authStatus =
        userId == "" ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
  }

  void _signedIn() {
    setState(() {
      authStatus = AuthStatus.NOT_DETERMINED;
    });
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        userId = user?.uid;
        curUser = user;
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void _signedOut() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      userId = null;
      curUser = null;
    });
  }

  Widget _waitingScreen() {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: backgroundGradient[1],
          gradient: LinearGradient(colors: backgroundGradient),
        ),
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_LOGGED_IN:
        return LoginScreen(
          auth: widget.auth,
          onSignedIn: _signedIn,
        );
      case AuthStatus.LOGGED_IN:
        if (curUser != null) {
          return Home(
            user: curUser,
            auth: widget.auth,
            onSignedOut: _signedOut,
          );
        } else return _waitingScreen();
        break;
      default:
        return _waitingScreen();
    }
  }
}
