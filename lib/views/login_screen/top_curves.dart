import 'package:flutter/material.dart';

import 'custom_clipper.dart';

class TopCurves extends StatefulWidget {
  @override
  State<TopCurves> createState() => _TopCurves();
}

class _TopCurves extends State<TopCurves> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: 300,
            color: Color(0xff4CA1AF),
//            child: Image.asset("assets/image/night.png"),
          ),
        )
      ],
    );
  }
}
