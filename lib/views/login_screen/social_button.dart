import 'package:flutter/material.dart';

class SocialButton extends StatelessWidget {

  final Color textColor, iconColor, backgroundColor, splashColor;

  final IconData icon;

  final double fontSize;

  final String text;

  final Function onPressed;

  const SocialButton({
    Key key,
    @required this.onPressed,
    @required this.backgroundColor,
    @required this.text,
    this.textColor = Colors.white,
    this.iconColor = Colors.white,
    this.splashColor,
    this.icon,
    this.fontSize = 16.0,
  }): assert(text != null),
      assert(icon != null),
      assert(textColor != null),
      assert(backgroundColor != null),
      assert(onPressed != null)
  ;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: 40,
      height: 35,
      onPressed: onPressed,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      color: backgroundColor,
      shape:  RoundedRectangleBorder(
        borderRadius:
        BorderRadius.all(Radius.circular(30)),
        ),
      child: Container(
        constraints: BoxConstraints(
          maxWidth: 220,
        ),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3, vertical: 3),
                child: Icon(
                  icon,
                  size: 24,
                  color: this.iconColor,
                ),
              ),
              SizedBox(width: 10,),
              Text(text,style: TextStyle(
                fontFamily: 'Poppins-Medium',
                color: textColor,
                fontSize: fontSize,
                backgroundColor: Color.fromRGBO(0, 0, 0, 0)
              ),)
            ],
          ),
        ),
      ),
    );
  }

}