import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/components/level_card/level_card.dart';
import 'package:gepochs/support/constants.dart';

const ExampleData = [
  {
    'index': 1,
    'title': 'Daenerys Targaryen',
    'imageUrl':
        'https://cdn.cnn.com/cnnnext/dam/assets/190503172554-daenerys-targaryen-game-of-thrones-exlarge-169.jpg'
  },
  {
    'index': 2,
    'title': 'Jon Snow',
    'imageUrl': 'https://i.ytimg.com/vi/t_jwAtCQ4-4/maxresdefault.jpg'
  },
  {
    'index': 3,
    'title': 'Night King',
    'imageUrl':
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSwxgvjRAa0i6L8PuMzXvn9-gIYZgT8QjY2K7v983U9XUEfk5DK'
  },
  {
    'index': 4,
    'title': 'Jaime Lannister',
    'imageUrl':
        'https://fsmedia.imgix.net/33/3f/c4/fb/6621/4588/a257/4abd3ac8ab25/jaime-lannister.jpeg?rect=0%2C39%2C1920%2C958&auto=format%2Ccompress&dpr=2&w=650'
  },
  {
    'index': 5,
    'title': 'Sansa Stark',
    'imageUrl':
        'https://media.glamour.com/photos/5cd0158fb33ad06050ca0753/16:9/w_2560%2Cc_limit/f35f9ff75e565d45ebb61aee0c9ff765c4a3a918354cb6fdc8b20120d16ccf1dd4b3be4febfe3db50ddb6c2f485d81bd.jpg'
  },
  {
    'index': 6,
    'title': 'Arya Stark',
    'imageUrl': 'https://i.ytimg.com/vi/E1sevzM6fxE/maxresdefault.jpg'
  },
  {
    'index': 7,
    'title': 'Tyrion Lannister',
    'imageUrl': 'https://miro.medium.com/max/2594/1*OtG2bOqXGBQYWqw7Tef_Tg.jpeg'
  },
  {
    'index': 8,
    'title': 'Cersei Lannister',
    'imageUrl':
        'https://image.iol.co.za/image/1/process/620x349?source=https://inm-baobab-prod-eu-west-1.s3.amazonaws.com/public/inm/media/image/97629268.JPG'
  },
  {
    'index': 9,
    'title': 'Bran Stark',
    'imageUrl': 'https://uproxx.com/wp-content/uploads/2019/04/bran-stark-1.jpg'
  },
  {
    'index': 10,
    'title': 'Stannis Baratheon',
    'imageUrl': 'https://i.ytimg.com/vi/h19o0QzuTP8/maxresdefault.jpg'
  },
];

//const backgroundGradient = [Color(0xFF141E30), Color(0xFF243B55)];

class RankingScreen extends StatefulWidget {
  @override
  _RankingScreenState createState() => _RankingScreenState();
}

class _RankingScreenState extends State<RankingScreen> {
  @override
  initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
//        appBar: AppBar(
//          title: Text("Ranking"),
//        ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: backgroundGradient)),
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      'Top Ranking',
                      style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Expanded(
                      child: Container(
                        padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
                        child: ListView(
                          children: ExampleData.map((item) => LevelCard(
                            title: item['title'],
                            index: item['index'],
                            imageUrl: item['imageUrl'],
                          )).toList(),
                        ),
                      )),
                ],
              ),
            ),
          ),
          BackNavigation(context),
        ],
      ),
    );
  }
}
