import 'dart:async';
import 'dart:io';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

enum TtsState { playing, stopped }
const RECURSIVE_TIME = 4;

class FlashCard extends StatefulWidget {
  final String word;
  final String meaning;
  final String imageUrl;

  FlashCard({Key key, this.word, this.meaning, this.imageUrl})
      : super(key: key);

  @override
  _FlashCardState createState() => new _FlashCardState();
}

class _FlashCardState extends State<FlashCard> {
  FlutterTts flutterTts;
  dynamic languages;
  dynamic voices;
  String _newVoiceText;
  int index = 0;
  List<Widget> _contents = [];

  TtsState ttsState = TtsState.stopped;

  get isPlaying => ttsState == TtsState.playing;

  get isStopped => ttsState == TtsState.stopped;

  @override
  initState() {
    super.initState();
    initTts();
    _contents.add(Text(
      widget.word,
      style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
    ));
    _contents.add(Text(
      widget.meaning,
      style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
    ));
    _contents.add(IconButton(
      icon: Icon(
        Icons.volume_up,
        size: 30,
      ),
      onPressed: () {
        _onChange(widget.word);
        _speak();
      },
    ));
    _contents.add(Image.asset(widget.imageUrl));
  }

  initTts() {
    flutterTts = FlutterTts();

    flutterTts.setLanguage("en-US");

    if (Platform.isAndroid) {
      flutterTts.ttsInitHandler(() {
        _getLanguages();
        _getVoices();
      });
    } else if (Platform.isIOS) {
      _getLanguages();
      _getVoices();
    }

    flutterTts.setStartHandler(() {
      setState(() {
        ttsState = TtsState.playing;
      });
    });

    flutterTts.setCompletionHandler(() {
      setState(() {
        ttsState = TtsState.stopped;
      });
    });

    flutterTts.setErrorHandler((msg) {
      setState(() {
        ttsState = TtsState.stopped;
      });
    });
  }

  Future _getLanguages() async {
    languages = await flutterTts.getLanguages;
    if (languages != null) setState(() => languages);
  }

  Future _getVoices() async {
    voices = await flutterTts.getVoices;
    if (voices != null) setState(() => voices);
  }

  Future _speak() async {
    if (_newVoiceText != null) {
      if (_newVoiceText.isNotEmpty) {
        var result = await flutterTts.speak(_newVoiceText);
        if (result == 1) setState(() => ttsState = TtsState.playing);
      }
    }
  }

  Future _stop() async {
    var result = await flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  void _onChange(String text) {
    setState(() {
      _newVoiceText = text;
    });
  }

  @override
  void dispose() {
    super.dispose();
    flutterTts.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlipCard(
          direction: FlipDirection.HORIZONTAL,
          onFlip: () {
            setState(() {
              if (index < 3) {
                index++;
              } else {
                index = 0;
              }
            });
          },
          front: Card(
            child: ListTile(
              leading: Column(
                children: <Widget>[
                  Image.asset("images/drag_left.png", width: 35, height: 35,),
                  Text("Vuốt trái", style: TextStyle(fontWeight: FontWeight.bold),)
                ],
              ),
              trailing: Column(
                children: <Widget>[
                  Image.asset("images/drag_right.png", width: 35, height: 35,),
                  Text("Vuốt phải", style: TextStyle(fontWeight: FontWeight.bold),)
                ],
              ),
              title: Container(
                width: 400,
                height: 600,
                child: Center(
                  child: _contents[index],
                ),
              ),
            ),
          ),
          back: Card(
            child: ListTile(
              leading: Column(
                children: <Widget>[
                  Image.asset("images/drag_left.png", width: 35, height: 35,),
                  Text("Vuốt trái", style: TextStyle(fontWeight: FontWeight.bold),)
                ],
              ),
              trailing: Column(
                children: <Widget>[
                  Image.asset("images/drag_right.png", width: 35, height: 35,),
                  Text("Vuốt phải", style: TextStyle(fontWeight: FontWeight.bold),)
                ],
              ),
              title: Container(
                width: 400,
                height: 600,
                child: Center(
                  child: _contents[index],
                ),
              ),
            ),
          )),
    );
  }
}
