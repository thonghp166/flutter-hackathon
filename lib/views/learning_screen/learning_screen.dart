import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/views/learning_screen/flash_card.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:gepochs/support/constants.dart';

var listAnimals = <FlashCard>[
  FlashCard(
    word: "dog",
    meaning: "chó",
    imageUrl: "images/dog.jpg",
  ),
  FlashCard(
    word: "cat",
    meaning: "mèo",
    imageUrl: "images/cat.jpg",
  ),
  FlashCard(
    word: "duck",
    meaning: "vịt",
    imageUrl: "images/duck.jpg",
  )
];

class LearningScreen extends StatefulWidget {
  @override
  LearningScreenState createState() => new LearningScreenState();
}

class LearningScreenState extends State<LearningScreen> {
  int _index = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
            decoration:
            BoxDecoration(gradient: LinearGradient(colors: backgroundGradient)),
            child: Swiper(
              onIndexChanged: (index) {
                setState(() {
                  _index = index + 1;
                });
              },
              itemBuilder: (BuildContext context, int index) {
                return listAnimals[index];
              },
              itemCount: 3,
              itemWidth: 400.0,
              itemHeight: 600.0,
              layout: SwiperLayout.TINDER,
            ),
          ),
          BackNavigationWithTitle(context, "${_index}/${listAnimals.length}"),
        ],
      ),
    );
  }
}
