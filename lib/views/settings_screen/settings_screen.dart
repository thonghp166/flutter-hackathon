import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/core/models/user.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/about_screen/about_screen.dart';
import 'package:gepochs/views/login_screen/authentication.dart';

class SettingsScreen extends StatefulWidget {
  final BaseAuth auth;
  final User user;

  const SettingsScreen({Key key, this.auth, this.user}) : super(key: key);
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: backgroundGradient)),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 32),
                  child: SizedBox(
                    height: 160.0,
                    child: Card(
                      elevation: 0,
                      color: Colors.transparent,
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              ClipRRect(
                                borderRadius: new BorderRadius.circular(50.0),
                                child: Image.network(
                                  widget.user.imgUrl,
                                  fit: BoxFit.fill,
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: Text(
                                  widget.user.name,
                                  style: TextStyle(
                                      color: mainColorText,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                              )
                            ],
                          )),
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(
                    Icons.book,
                    color: Colors.blue,
                    size: 32.0,
                  ),
                  title: Text("Sách đã học",
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: mainColorText)),
                  subtitle: Text(
                    "Số lượng sách hoàn thành trên 80%",
                    style: TextStyle(color: subColorText),
                  ),
                  trailing: Text(
                    "100/169",
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  indent: 16,
                ),
                ListTile(
                  leading: Icon(
                    Icons.sort_by_alpha,
                    color: Colors.blue,
                    size: 32.0,
                  ),
                  title: Text("Số từ mới đã học",
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: mainColorText)),
                  subtitle: Text(
                    "Số lượng từ mới được học thêm",
                    style: TextStyle(color: subColorText),
                  ),
                  trailing: Text(
                    "1024",
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  indent: 16,
                ),
                ListTile(
                  leading: Icon(
                    Icons.mic_none,
                    color: Colors.blue,
                    size: 32.0,
                  ),
                  title: Text("Đánh giá phát âm",
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: mainColorText)),
                  subtitle: Text(
                    "So với phát âm người bản ngữ",
                    style: TextStyle(color: subColorText),
                  ),
                  trailing: Text(
                    "Good",
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  indent: 16,
                ),
                ListTile(
                  leading: Icon(
                    Icons.info,
                    color: Colors.blue,
                    size: 32.0,
                  ),
                  title: Text('Giới thiệu',style: TextStyle(
                      fontWeight: FontWeight.w400, color: mainColorText)),
                  trailing: Text(
                    "GEpochs",
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AboutScreen()),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        BackNavigation(context)
      ],
    ));
  }
}
