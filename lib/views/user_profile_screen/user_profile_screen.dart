import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/core/models/user.dart';
import 'package:gepochs/views/login_screen/authentication.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/support/constants.dart';

class UserProfileScreen extends StatefulWidget {
  final BaseAuth auth;
  final User user;

  const UserProfileScreen({Key key, this.auth, this.user}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfileScreen> {
  bool _isPushNotification = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(32, 64, 32, 32),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: backgroundGradient)),
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
              ),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(0, 32, 0, 32),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 160.0,
                child: Card(
                  elevation: 0,
                  color: Colors.transparent,
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: new BorderRadius.circular(50.0),
                        child: Image.network(
                          widget.user.imgUrl,
                          fit: BoxFit.fill,
                          width: 100,
                          height: 100,
                        ),
                      ),
                    ],
                  )),
                ),
              ),
              SizedBox(
                height: 50,
                child: ListTile(
                  leading: Icon(
                    Icons.person,
                    color: Colors.blue,
                    size: 22.0,
                  ),
                  title: Text("Name",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white)),
                  trailing: Text(
                    widget.user.name,
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                  onTap: () {},
                ),
              ),
              Divider(
                height: 5,
                color: Colors.grey,
                indent: 16,
              ),
              SizedBox(
                height: 50,
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.blue,
                    size: 22.0,
                  ),
                  title: Text("Email",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white)),
                  trailing: Text(
                    widget.user.email,
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                  onTap: () {},
                ),
              ),
              Divider(
                height: 5,
                color: Colors.grey,
                indent: 16,
              ),
              SizedBox(
                height: 50,
                child: ListTile(
                  leading: Icon(
                    Icons.notifications,
                    color: Colors.blue,
                    size: 22.0,
                  ),
                  title: Text("Notification",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white)),
                  trailing: Switch(
                    onChanged: (val) =>
                        setState(() => _isPushNotification = val),
                    value: _isPushNotification,
                    activeColor: Colors.green,
                  ),
                  onTap: () {
                    setState(() {
                      _isPushNotification = !_isPushNotification;
                    });
                  },
                ),
              ),
              Divider(
                height: 5,
                color: Colors.grey,
                indent: 16,
              ),
              SizedBox(
                height: 50,
                child: ListTile(
                  leading: Icon(
                    FontAwesomeIcons.facebook,
                    color: Colors.blue,
                    size: 22.0,
                  ),
                  title: Text("Facebook",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white)),
                  trailing: Text(
                    "Kết nối tài khoản",
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                  onTap: () {},
                ),
              ),
              Divider(
                height: 5,
                color: Colors.grey,
                indent: 16,
              ),
              SizedBox(
                height: 50,
                child: ListTile(
                  leading: Icon(
                    FontAwesomeIcons.google,
                    color: Colors.blue,
                    size: 22.0,
                  ),
                  title: Text("Google",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white)),
                  trailing: Text(
                    "Kết nối tài khoản",
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                  onTap: () {},
                ),
              ),
              Divider(
                height: 5,
                color: Colors.grey,
                indent: 16,
              ),
            ],
          ),
        ),
        BackNavigation(context),
      ],
    ));
  }
}
