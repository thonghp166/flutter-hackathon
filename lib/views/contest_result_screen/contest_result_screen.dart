import 'package:flutter/material.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/support/constants.dart';

class ContestResultScreen extends StatefulWidget {
  @override
  _ContestResultState createState() => _ContestResultState();
}

class _ContestResultState extends State<ContestResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(32, 16, 32, 32),
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: backgroundGradient)),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      'Contest Results',
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
//                  Expanded(
//                    child: ListView(
//                      children: ExampleData.map((item) => LevelCard(
//                        title: item['title'],
//                        index: item['index'],
//                        imageUrl: item['imageUrl'],
//                      )).toList(),
//                    ),
//                  ),
                ],
              ),
            ),
            BackNavigation(context)
          ],
        ));
  }
}
