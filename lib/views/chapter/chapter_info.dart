import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gepochs/components/back_navigation/back_natigation.dart';
import 'package:gepochs/views/home/home.dart';
import 'package:gepochs/views/learning_screen/learning_screen.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/quiz/quiz_exercise.dart';
import 'package:gepochs/views/start_contest_screen/start_contest_screen.dart';

class ChapterInfo extends StatelessWidget {
  final int index;
  final String title;
  final String imageUrl;
  final Color color;

  ChapterInfo({this.title, this.imageUrl, this.index, this.color});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: backgroundGradient,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              child: Align(
                alignment: Alignment.topCenter,
                child: Stack(
                  children: [
                    Container(
                      height: 180,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(imageUrl),
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Opacity(
                      opacity: 0.5,
                      child: Container(
                        height: 180,
                        decoration: BoxDecoration(
//                          borderRadius: BorderRadius.only(
//                            bottomRight: Radius.circular(90),
//                          ),
                          color: Colors.black,
                        ),
                      ),
                    ),
                    BackNavigation(context),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 180, 0, 0),
              decoration: BoxDecoration(
                border: Border(
//                  bottom: BorderSide(
//                    color: Colors.black38,
//                    width: 4,
//                  ),
//                  top: BorderSide(
//                    color: Colors.black38,
//                    width: 4,
//                  ),
                    ),
              ),
              child: ListView(
                padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                  ),
                  Text(
                    'Some very long description about chapter $index',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: Divider(
                      color: color,
                      thickness: 2,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            LessonItem(
                              title: 'Lesson 1',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 2',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 3',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 4',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 5',
                              color: color,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            LessonItem(
                              title: 'Lesson 6',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 7',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 8',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 9',
                              color: color,
                            ),
                            LessonItem(
                              title: 'Lesson 10',
                              color: color,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Center(
                    child: Divider(
                      color: color,
                      thickness: 2,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: Container(
                        width: MediaQuery.of(context).size.width * 2 / 3,
                        child: Text(
                          'Are you ready to take the ultimate test?',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        )),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: Container(
                      width: 240,
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        boxShadow: box_shadow_2,
                      ),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        onPressed: () => Navigator.push(context,
                            MaterialPageRoute(builder: (context) => StartContestScreen())),
                        color: color,
                        child: Center(
                          child: Text(
                            'Take the test',
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 48,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LessonItem extends StatelessWidget {
  final String title;
  final bool completed;
  final Color color;

  LessonItem({this.title, this.completed, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      child: GestureDetector(
        onTap: () => {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => LearningScreen()
          )),
        },
        child: Column(
          children: <Widget>[
            Center(
              child: Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: backgroundGradient),
                  boxShadow: box_shadow_2,
                  shape: BoxShape.circle,
                  border: Border.all(color: color, width: 2),
                ),
                child: Center(
                  child: Icon(
                    FontAwesomeIcons.book,
                    color: Colors.white,
                    size: 24,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Center(
              child: Text(
                title,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
