import 'package:flutter/material.dart';

const backgroundGradient = [Color(0xFF141E30), Color(0xFF243B55)];
const mainColorText = Colors.white;
const subColorText = Colors.grey;
const iconColor = Colors.white;
const splashColors = [
  Colors.redAccent,
  Colors.blueAccent,
  Colors.greenAccent,
  Colors.yellowAccent,
  Colors.pinkAccent,
  Colors.purpleAccent,
  Colors.cyanAccent,
  Colors.orangeAccent,
  Colors.lightBlueAccent,
  Colors.tealAccent,
];
const borderRadiusDefaul = BorderRadius.all(Radius.circular(12));
const imageStarsBackground = "https://www.searchpng.com/wp-content/uploads/2019/01/Gold-Confetti-Transparent-Clip-Art-715x715.png";
const iconMedal = "https://i-love-png.com/images/1938a7b89c.png";
const imageCgvTickets = "https://www.cgv.vn/media/wysiwyg/giftcard/gift-card-2.png";
const continueButtonColor = Colors.redAccent;
const buttonTextColor = Colors.white;
const buttonGradient = [Color(0xFFff416c), Color(0xFFff4b2b)];
const iconButtonColor = Colors.blueGrey;
const currentLessonColor = Colors.black;
const browseButtonColor = Colors.redAccent;
const completionIconBorderColor = Colors.green;
const coinsIconColor = Colors.orange;
const headerTextColor = Colors.white;
