// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lesson.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Lesson _$LessonFromJson(Map<String, dynamic> json) {
  return Lesson(
    name: json['name'] as String,
    chapter: json['chapter'] as String,
    number: json['number'] as int,
  );
}

Map<String, dynamic> _$LessonToJson(Lesson instance) => <String, dynamic>{
      'name': instance.name,
      'chapter': instance.chapter,
      'number': instance.number,
    };
