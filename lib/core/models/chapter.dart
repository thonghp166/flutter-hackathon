import 'package:json_annotation/json_annotation.dart';

part 'chapter.g.dart';

@JsonSerializable(nullable: false)
class Chapter {
  final String name;
  final int number;

  Chapter({this.name, this.number});

  factory Chapter.fromJson(Map<String, dynamic> json) =>
      _$ChapterFromJson(json);

  Map<String, dynamic> toJson() => _$ChapterToJson(this);
}
