import 'package:flutter/cupertino.dart';

class User {
  final String id;
  final String name;
  final String uid;
  final int coin;
  final String token;
  final int ticket;
  final String provider;
  final String imgUrl;
  final String email;

  User({this.id, this.email, this.imgUrl, this.provider, this.ticket,  this.name, this.coin, this.token, this.uid});

  Map<String, dynamic> toJson(){
    return {
      "id": this.id,
      "provider": this.provider,
      "email": this.email,
      "uid": this.uid,
      "name": this.name,
      "imgUrl": this.imgUrl,
      "coin" : this.coin,
      "token": this.token
    };
  }

}

class SocialId {
  final String provider;
  final String id;

  SocialId({
    @required this.provider,
    @required this.id
  });

  Map<String, dynamic> toJson() {
    return {
      "provider": this.provider,
      "id": this.id
    };
  }

  factory SocialId.fromJson(Map<String,dynamic> json) {
    return SocialId(
      provider: json["provider"],
      id: json["id"]
    );
  }
}