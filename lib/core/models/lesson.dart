import 'package:json_annotation/json_annotation.dart';

part 'lesson.g.dart';

@JsonSerializable(nullable: false)
class Lesson {
  final String name;
  final String chapter;
  final int number;

  Lesson({this.name, this.chapter, this.number});

  factory Lesson.fromJson(Map<String, dynamic> json) => _$LessonFromJson(json);

  Map<String, dynamic> toJson() => _$LessonToJson(this);
}