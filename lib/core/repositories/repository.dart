abstract class Repository {
  final String collection;

  Repository({this.collection});
}