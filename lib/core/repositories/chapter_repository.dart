import 'package:gepochs/core/models/chapter.dart';
import 'package:gepochs/core/models/lesson.dart';
import 'package:gepochs/core/repositories/repository.dart';
import 'package:gepochs/core/utilities/http_service.dart';

class ChapterRepository extends Repository {
  ChapterRepository() : super(collection: 'chapters');

  Future<List<Chapter>> getChapters() async {
    var response =
        (await HttpService.get(endpoint: collection))['data']['data'] as List;
    return new List<Chapter>.from(response.map((c) => Chapter.fromJson(c)));
  }

  Future<List<Lesson>> getLessons(String chapterId) async {
    var response = (await HttpService.get(
            endpoint: collection + '/' + chapterId + '/lessons'))['data']
        ['data'] as List;
    return new List<Lesson>.from(response.map((l) => Lesson.fromJson(l)));
  }
}
