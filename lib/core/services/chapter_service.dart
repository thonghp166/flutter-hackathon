import 'package:gepochs/core/models/chapter.dart';
import 'package:gepochs/core/models/lesson.dart';
import 'package:gepochs/core/repositories/chapter_repository.dart';

class ChapterService {
  static final ChapterService instance = ChapterService._();

  final ChapterRepository repository;

  ChapterService._() : repository = ChapterRepository();

  Future<List<Chapter>> getChapters() => repository.getChapters();

  Future<List<Lesson>> getLessons(String chapterId) => repository.getLessons(chapterId);
}
