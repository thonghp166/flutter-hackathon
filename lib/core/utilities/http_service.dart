import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class HttpService {
  static const _host = 'https://sheltered-castle-82867.herokuapp.com/';

  static dynamic get({@required String endpoint, Map params}) async {
    var query = '';
    if (params != null) {
      query = '?';
      params.forEach((key, value) {
        query += key + '=' + value;
      });
    }
    var response = await http.get(_host + endpoint + query);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return null;
    }
  }

  static dynamic post({@required String endpoint, Map<String, dynamic> body}) async {
    var response = await http.post(_host + endpoint, body: jsonEncode(body), headers: {"Content-Type": "application/json"});
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return null;
    }
  }
}
