import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gepochs/views/login_screen/authentication.dart';
import 'package:gepochs/views/login_screen/root_page.dart';


void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      title: 'GEpochs',
      theme: ThemeData(fontFamily: 'Montserrat'),
      debugShowCheckedModeBanner: false,
      home: NavigatorHome(),
    );
  }
}

class NavigatorHome extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return Scaffold(body: RootPage(auth: new Auth(),));
  }

}
