import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:gepochs/support/constants.dart';
import 'package:gepochs/views/detail_ranking_screen/detail_ranking_screen.dart';

const borderRadius = BorderRadius.all(Radius.circular(16));

class LevelCard extends StatelessWidget {
  final int index;
  final String title;
  final String imageUrl;
  final Color backgroundColor;

  LevelCard({this.index, this.title, this.imageUrl, this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    Color colorText = mainColorText;
    if (index == 1) {
      colorText = Colors.red;
    } else if (index == 2) {
      colorText = Colors.orange;
    } else if (index == 3) {
      colorText = Colors.yellow;
    }

    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailRankingScreen(
//                      index: index,
//                      title: title,
//                      imageUrl: imageUrl,
                  ))),
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
        height: 80,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
        ),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: borderRadius,
                color: Color(0xff2E2A5E),
//              image: DecorationImage(
//                image: NetworkImage(imageUrl),
//                fit: BoxFit.cover,
//              ),
              ),
            ),
            Opacity(
              opacity: 0.5,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: borderRadius,
                  color: Colors.black,
                ),
              ),
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 64,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(16.0),
                            bottomLeft: const Radius.circular(16.0))),
                    child: Center(
                      child: Text("$index",
                          style: TextStyle(
                              color: colorText,
                              fontSize: 36.0,
                              fontWeight: FontWeight.w600)),
                    ),
                  ),
                ),
                SizedBox(
                  width: 3,
                  child: Container(
//                  color: Color(0xff17253a),
                    color: Colors.black26,
                  ),
                ),
                SizedBox(
                    width: 64,
                    child: Center(
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(50.0),
                        child: Image.network(
                          imageUrl,
                          fit: BoxFit.cover,
                          width: 48,
                          height: 48,
                        ),
                      ),
                    )),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      '$title',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      softWrap: false,
                      overflow: TextOverflow.clip,
                    ),
                  ),
                ),
                Container(
                  width: 60,
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
                  child: Center(
                    child: Text(
                      "9999",
                      softWrap: false,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          color: mainColorText,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
