import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget BackNavigation(context) {
  return SafeArea(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.white,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget BackNavigationWithTitle(context, title) {
  return SafeArea(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.white,
            ),
          ),
        ),
        Expanded(
          child: Text(title, style: TextStyle(color: Colors.white, fontSize: 25),),
        )
      ],
    ),
  );
}
